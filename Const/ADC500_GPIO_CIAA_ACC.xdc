set_property IOSTANDARD LVCMOS25 [get_ports {HV_CTRL[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {LED_1[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {RANGE_2[0]}]
set_property IOSTANDARD LVCMOS25 [get_ports {RANGE_1[0]}]

set_property PACKAGE_PIN AE20 [get_ports {RANGE_1[0]}]
set_property PACKAGE_PIN AC17 [get_ports {RANGE_2[0]}]
set_property PACKAGE_PIN Y20 [get_ports {LED_1[0]}]
set_property PACKAGE_PIN AC22 [get_ports {HV_CTRL[0]}]

set_property DRIVE 16 [get_ports {HV_CTRL[0]}]
set_property DRIVE 16 [get_ports {LED_1[0]}]
set_property DRIVE 16 [get_ports {RANGE_2[0]}]
set_property DRIVE 16 [get_ports {RANGE_1[0]}]
set_property PULLUP true [get_ports {HV_CTRL[0]}]
set_property PULLUP true [get_ports {LED_1[0]}]
set_property PULLUP true [get_ports {RANGE_2[0]}]
set_property PULLUP true [get_ports {RANGE_1[0]}]

set_property SLEW SLOW [get_ports {HV_CTRL[0]}]
set_property SLEW SLOW [get_ports {LED_1[0]}]
set_property SLEW SLOW [get_ports {RANGE_2[0]}]
set_property SLEW SLOW [get_ports {RANGE_1[0]}]








