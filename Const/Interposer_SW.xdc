set_property PACKAGE_PIN N3 [get_ports {PAMP_ID[0]}]
set_property PACKAGE_PIN N2 [get_ports {PAMP_ID[1]}]
set_property PACKAGE_PIN M6 [get_ports {PAMP_ID[2]}]
set_property PACKAGE_PIN M5 [get_ports {PAMP_ID[3]}]
set_property PACKAGE_PIN M2 [get_ports {PAMP_ID[4]}]
set_property PACKAGE_PIN L2 [get_ports {PAMP_ID[5]}]
set_property PACKAGE_PIN N4 [get_ports {PAMP_ID[6]}]
set_property PACKAGE_PIN M4 [get_ports {PAMP_ID[7]}]

set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[3]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[4]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[5]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[6]}]
set_property IOSTANDARD LVCMOS18 [get_ports {PAMP_ID[7]}]

set_property PACKAGE_PIN D6 [get_ports HV_SCLK]
set_property PACKAGE_PIN C6 [get_ports HV_MOSI]
set_property PACKAGE_PIN B9 [get_ports HV_nON]
set_property PACKAGE_PIN E1 [get_ports {HV_MISO[0]}]
set_property PACKAGE_PIN C9 [get_ports {HV_MISO[1]}]
# set_property PACKAGE_PIN A9 [get_ports {HV_CS[0]}] #VSET
# set_property PACKAGE_PIN E2 [get_ports {HV_CS[1]}] #VMON
# set_property PACKAGE_PIN A8 [get_ports {HV_CS[2]}] #IMON

set_property IOSTANDARD LVCMOS18 [get_ports HV_SCLK]
set_property IOSTANDARD LVCMOS18 [get_ports HV_MOSI]
set_property IOSTANDARD LVCMOS18 [get_ports HV_nON]
set_property IOSTANDARD LVCMOS18 [get_ports {HV_MISO[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {HV_MISO[1]}]
# set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[0]}] #VSET
# set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[1]}] #VMON
# set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[2]}] #IMON

set_property PACKAGE_PIN E2 [get_ports {HV_CS[0]}]
set_property PACKAGE_PIN A8 [get_ports {HV_CS[1]}]
set_property PACKAGE_PIN A9 [get_ports {HV_CS[2]}]
set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {HV_CS[2]}]

set_property IOSTANDARD LVCMOS18 [get_ports {ECALHV_CS[1]}]
set_property IOSTANDARD LVCMOS18 [get_ports {ECALHV_CS[0]}]
set_property IOSTANDARD LVCMOS18 [get_ports ECALHV_DATA]
set_property IOSTANDARD LVCMOS18 [get_ports ECALHV_SCLK]
set_property IOSTANDARD LVCMOS18 [get_ports {EXT_TRIG[0]}]

set_property PACKAGE_PIN L8 [get_ports {ECALHV_CS[0]}]
set_property PACKAGE_PIN M8 [get_ports {ECALHV_CS[1]}]
set_property PACKAGE_PIN G1 [get_ports ECALHV_DATA]
set_property PACKAGE_PIN H2 [get_ports ECALHV_SCLK]
set_property PACKAGE_PIN A17 [get_ports {EXT_TRIG[0]}]

set_property SLEW SLOW [get_ports {ECALHV_CS[0]}]
set_property SLEW SLOW [get_ports ECALHV_DATA]
set_property SLEW SLOW [get_ports ECALHV_SCLK]

set_property DRIVE 4 [get_ports {ECALHV_CS[1]}]
set_property DRIVE 4 [get_ports {ECALHV_CS[0]}]
set_property DRIVE 4 [get_ports ECALHV_DATA]
set_property DRIVE 4 [get_ports ECALHV_SCLK]

set_property PACKAGE_PIN A12 [get_ports {ecal_led}]
set_property IOSTANDARD LVCMOS18 [get_ports {ecal_led}]




set_property PULLUP true [get_ports {HV_CS[2]}]
set_property PULLUP true [get_ports {HV_CS[1]}]
set_property PULLUP true [get_ports {HV_CS[0]}]
set_property PULLUP true [get_ports {HV_MISO[1]}]
set_property PULLUP true [get_ports {HV_MISO[0]}]
set_property PULLUP true [get_ports HV_MOSI]
set_property PULLUP true [get_ports HV_SCLK]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk]
