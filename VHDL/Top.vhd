--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.1.3 (lin64) Build 2644227 Wed Sep  4 09:44:18 MDT 2019
--Date        : Wed Oct 13 22:57:09 2021
--Host        : kbonovo running 64-bit Ubuntu 20.04.3 LTS
--Command     : generate_target Top_wrapper.bd
--Design      : Top_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Top_wrapper is
  port (
    A5GPIO : out STD_LOGIC_VECTOR ( 6 downto 0 );
    CLK_IN_D_N : in STD_LOGIC;
    CLK_IN_D_P : in STD_LOGIC;
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    ECALHV_CS : out STD_LOGIC_VECTOR ( 1 downto 0 );
    ECALHV_DATA : out STD_LOGIC;
    ECALHV_SCLK : out STD_LOGIC;
    EXT_TRIG : in STD_LOGIC_VECTOR ( 0 to 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    HV_CS : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HV_CTRL : out STD_LOGIC;
    HV_MISO : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HV_MOSI : out STD_LOGIC;
    HV_SCLK : out STD_LOGIC;
    HV_nON : out STD_LOGIC;
    LED_1 : out STD_LOGIC;
    PAMP_ID : in STD_LOGIC_VECTOR ( 7 downto 0 );
    RANGE_1 : out STD_LOGIC;
    RANGE_2 : out STD_LOGIC;
    VADJ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_N : out STD_LOGIC;
    clk_to_adc_DS_P : out STD_LOGIC;
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ecal_led : out STD_LOGIC;
    sclk : out STD_LOGIC;
    sdata : inout STD_LOGIC;
    ss : out STD_LOGIC_VECTOR ( 2 downto 0 );
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_from_adc_or_n : in STD_LOGIC;
    x_from_adc_or_p : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC
  );
end Top_wrapper;

architecture STRUCTURE of Top_wrapper is
  component Top is
  port (
    A5GPIO : out STD_LOGIC_VECTOR ( 6 downto 0 );
    CLK_IN_D_N : in STD_LOGIC;
    CLK_IN_D_P : in STD_LOGIC;
    EXT_TRIG : in STD_LOGIC_VECTOR ( 0 to 0 );
    HV_CS : out STD_LOGIC_VECTOR ( 2 downto 0 );
    HV_CTRL : out STD_LOGIC;
    HV_MISO : in STD_LOGIC_VECTOR ( 1 downto 0 );
    HV_MOSI : out STD_LOGIC;
    HV_SCLK : out STD_LOGIC;
    HV_nON : out STD_LOGIC;
    LED_1 : out STD_LOGIC;
    PAMP_ID : in STD_LOGIC_VECTOR ( 7 downto 0 );
    RANGE_1 : out STD_LOGIC;
    RANGE_2 : out STD_LOGIC;
    VADJ : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_N : out STD_LOGIC;
    clk_to_adc_DS_P : out STD_LOGIC;
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    sclk : out STD_LOGIC;
    sdata : inout STD_LOGIC;
    ss : out STD_LOGIC_VECTOR ( 2 downto 0 );
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_from_adc_or_n : in STD_LOGIC;
    x_from_adc_or_p : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC;
    ECALHV_DATA : out STD_LOGIC;
    ECALHV_SCLK : out STD_LOGIC;
    ECALHV_CS : out STD_LOGIC_VECTOR ( 1 downto 0 );
    DDR_cas_n : inout STD_LOGIC;
    DDR_cke : inout STD_LOGIC;
    DDR_ck_n : inout STD_LOGIC;
    DDR_ck_p : inout STD_LOGIC;
    DDR_cs_n : inout STD_LOGIC;
    DDR_reset_n : inout STD_LOGIC;
    DDR_odt : inout STD_LOGIC;
    DDR_ras_n : inout STD_LOGIC;
    DDR_we_n : inout STD_LOGIC;
    DDR_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_ps_srstb : inout STD_LOGIC;
    FIXED_IO_ps_clk : inout STD_LOGIC;
    FIXED_IO_ps_porb : inout STD_LOGIC;
    ecal_led : out STD_LOGIC
  );
  end component Top;
begin
Top_i: component Top
     port map (
      A5GPIO(6 downto 0) => A5GPIO(6 downto 0),
      CLK_IN_D_N => CLK_IN_D_N,
      CLK_IN_D_P => CLK_IN_D_P,
      DDR_addr(14 downto 0) => DDR_addr(14 downto 0),
      DDR_ba(2 downto 0) => DDR_ba(2 downto 0),
      DDR_cas_n => DDR_cas_n,
      DDR_ck_n => DDR_ck_n,
      DDR_ck_p => DDR_ck_p,
      DDR_cke => DDR_cke,
      DDR_cs_n => DDR_cs_n,
      DDR_dm(3 downto 0) => DDR_dm(3 downto 0),
      DDR_dq(31 downto 0) => DDR_dq(31 downto 0),
      DDR_dqs_n(3 downto 0) => DDR_dqs_n(3 downto 0),
      DDR_dqs_p(3 downto 0) => DDR_dqs_p(3 downto 0),
      DDR_odt => DDR_odt,
      DDR_ras_n => DDR_ras_n,
      DDR_reset_n => DDR_reset_n,
      DDR_we_n => DDR_we_n,
      ECALHV_CS(1 downto 0) => ECALHV_CS(1 downto 0),
      ECALHV_DATA => ECALHV_DATA,
      ECALHV_SCLK => ECALHV_SCLK,
      EXT_TRIG(0) => EXT_TRIG(0),
      FIXED_IO_ddr_vrn => FIXED_IO_ddr_vrn,
      FIXED_IO_ddr_vrp => FIXED_IO_ddr_vrp,
      FIXED_IO_mio(53 downto 0) => FIXED_IO_mio(53 downto 0),
      FIXED_IO_ps_clk => FIXED_IO_ps_clk,
      FIXED_IO_ps_porb => FIXED_IO_ps_porb,
      FIXED_IO_ps_srstb => FIXED_IO_ps_srstb,
      HV_CS(2 downto 0) => HV_CS(2 downto 0),
      HV_CTRL => HV_CTRL,
      HV_MISO(1 downto 0) => HV_MISO(1 downto 0),
      HV_MOSI => HV_MOSI,
      HV_SCLK => HV_SCLK,
      HV_nON => HV_nON,
      LED_1 => LED_1,
      PAMP_ID(7 downto 0) => PAMP_ID(7 downto 0),
      RANGE_1 => RANGE_1,
      RANGE_2 => RANGE_2,
      VADJ(0) => VADJ(0),
      clk_to_adc_DS_N => clk_to_adc_DS_N,
      clk_to_adc_DS_P => clk_to_adc_DS_P,
      data_from_adc_DS_N(15 downto 0) => data_from_adc_DS_N(15 downto 0),
      data_from_adc_DS_P(15 downto 0) => data_from_adc_DS_P(15 downto 0),
      ecal_led => ecal_led,
      sclk => sclk,
      sdata => sdata,
      ss(2 downto 0) => ss(2 downto 0),
      x_from_adc_calrun_fmc => x_from_adc_calrun_fmc,
      x_from_adc_or_n => x_from_adc_or_n,
      x_from_adc_or_p => x_from_adc_or_p,
      x_to_adc_cal_fmc => x_to_adc_cal_fmc,
      x_to_adc_caldly_nscs_fmc => x_to_adc_caldly_nscs_fmc,
      x_to_adc_dclk_rst_fmc => x_to_adc_dclk_rst_fmc,
      x_to_adc_fsr_ece_fmc => x_to_adc_fsr_ece_fmc,
      x_to_adc_led_0 => x_to_adc_led_0,
      x_to_adc_led_1 => x_to_adc_led_1,
      x_to_adc_outedge_ddr_sdata_fmc => x_to_adc_outedge_ddr_sdata_fmc,
      x_to_adc_outv_slck_fmc => x_to_adc_outv_slck_fmc,
      x_to_adc_pd_fmc => x_to_adc_pd_fmc
    );
end STRUCTURE;
