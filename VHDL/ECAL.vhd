----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.11.2020 18:48:19
-- Design Name: 
-- Module Name: ECAL_HV_CTRL - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--   Notes: SPI_CFG_REG[31..0]
--   SPI_CFG[31..24]  CLK_DIV
--   SPI_CFG[23..16]  ADDR
--   SPI_CFG[3]       I/O   high= input; low= output
--   SPI_CFG[2]       cont  Continous mode high= continous low= single
--   SPI_CFG[1]       cpha  phase
--   SPI_CFG[0]       pol   polarity  


----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity ECAL_HV_CTRL is
    generic(N_PMT_MOD : Integer :=2); --Number of PMOD CTRL Modules
    Port (
      CLK   : IN     STD_LOGIC;                                  --system clock
      RSTN  : IN     STD_LOGIC;                                  --asynchronous reset          
      
    -- FPGA Ports
      SPI_CFG_REG : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register
      
      
      PMT_ADDR    : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register  
      SPI_TX  : IN     STD_LOGIC_VECTOR(31 DOWNTO 0);        --data to transmit
      
      enable_tx  : IN     STD_LOGIC;                             --initiate transaction
      busy    : OUT    STD_LOGIC;                                --busy / data ready signal

      --External ports
      HV_DATA    : OUT    STD_LOGIC;                            --master out, slave in
      HV_SCLK    : out   STD_LOGIC;                             --spi clock
      HV_CS      : out   STD_LOGIC_VECTOR(N_PMT_MOD -1 DOWNTO 0)       --slave select      
      );                             
end ECAL_HV_CTRL;

architecture Behavioral of ECAL_HV_CTRL is

TYPE CFG_REG is record
    addr        : std_logic_vector(31 downto 0);
    clk_div     : std_logic_vector(31 downto 0);
    cpol        : std_logic;
    cpha        : std_logic;
    cont        : std_logic;
    io          : std_logic;
end record CFG_REG;

signal SPI : CFG_REG;

    COMPONENT spi_master IS
    GENERIC(
      slaves  : INTEGER := 2;  --number of spi slaves
      d_width : INTEGER := 16); --data bus width
    PORT(
      clock   : IN     STD_LOGIC;                             --system clock
      reset_n : IN     STD_LOGIC;                             --asynchronous reset
      enable  : IN     STD_LOGIC;                             --initiate transaction
      cpol    : IN     STD_LOGIC;                             --spi clock polarity
      cpha    : IN     STD_LOGIC;                             --spi clock phase
      cont    : IN     STD_LOGIC;                             --continuous mode command
      clk_div_i : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --system clock cycles per 1/2 period of sclk
      addr_i    : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --address of slave
      tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
      miso    : IN     STD_LOGIC;                             --master in, slave out
      sclk    : BUFFER STD_LOGIC;                             --spi clock
      ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   --slave select
      mosi    : OUT    STD_LOGIC;                             --master out, slave in
      busy    : OUT    STD_LOGIC;                             --busy / data ready signal
      rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0)); --data received
    END COMPONENT spi_master;


signal enable_tx_sig, busy_sig : std_logic;
signal data_out, spi_clk_sig : std_logic; -- data output of two spi 
signal tx_data : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal ss_n : std_logic_vector(N_PMT_MOD -1 DOWNTO 0);
signal SET_PMT_ADDR : std_logic;
type state is (idle, bsy, wfl); --wfl stands for wait for low 
signal cstate, nstate : state;



begin

    --register setting
    SPI.cpol    <= SPI_CFG_REG(0);
    SPI.cpha    <= SPI_CFG_REG(1);
    SPI.cont    <= SPI_CFG_REG(2);
    SPI.io      <= SPI_CFG_REG(3);
    SET_PMT_ADDR<= SPI_CFG_REG(4);

    SPI.clk_div <= x"000000" & SPI_CFG_REG(23 DOWNTO 16);
    SPI.addr    <= x"000000" & SPI_CFG_REG(31 DOWNTO 24);


    tx_data<=   (PMT_ADDR(15 downto 0) and x"00ff") when SET_PMT_ADDR = '1' else
                SPI_TX(15 downto 0);
                
    process(clk, RSTN)
    begin
        if rstn = '0' then 
            cstate<=idle;
        elsif rising_edge(clk) then
            cstate<=nstate;
        end if;  
    end process;
    
    process(cstate)
    begin 
        case cstate is
            when idle =>
                enable_tx_sig<=enable_tx;
            when bsy =>
                enable_tx_sig<='0';
            when wfl =>
                enable_tx_sig<='0';
        end case;                
    end process;
    
    process(cstate, enable_tx, busy_sig)
    begin
        case cstate is
            when idle =>
                if busy_sig = '1' then
                    nstate<=bsy;
                else
                    nstate<=idle;
                end if;
            when bsy =>
                if SPI.cont = '1' then
                    nstate<=idle;
                elsif busy_sig = '0' then
                    nstate<=wfl;
                else
                    nstate<=bsy;
                end if;
            when wfl =>
                if SPI.cont = '1' or enable_tx ='0' then
                    nstate<=idle;
                else
                    nstate<=wfl;
                end if;
         end case;              
    end process;
    
    
    
    
    DAC_SPI: spi_master
    GENERIC MAP(
      slaves  => N_PMT_MOD, 
      d_width => 16
    )
    PORT MAP(
      clock   => CLK,
      reset_n => RSTN,
      enable  => enable_tx_sig,
      cpol    => SPI.cpol,
      cpha    => SPI.cpha,
      cont    => SPI.cont,
      clk_div_i => SPI.clk_div, 
      addr_i    => SPI.addr,
      tx_data => tx_data, 
      miso    => '0',
      sclk    =>spi_clk_sig,
      ss_n    =>ss_n, 
      mosi    =>data_out,
      busy    =>busy_sig,
      rx_data =>open
    );
     HV_DATA <= not data_out;
        
     HV_SCLK <= spi_clk_sig;
     
     HV_CS  <= not ss_n when SET_PMT_ADDR = '0' else
               (others=>'0');
    
     busy<=busy_sig;

end Behavioral;