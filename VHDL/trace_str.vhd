----------------------------------------------------------------------------------
--! @file trc_store.vhd
--! @brief 
-- Company: ICTP
-- Engineer: L. G. Garcia  
-- 
-- Create Date: 21.10.2019 11:56:00
-- Design Name: trace_str.vhd
-- Module Name: Trace Store - Behavioral
-- Project Name: INFN_PAMP
-- Target Devices: CIAA_ACC, ZEDBOARD ZYNQ 7020
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - max_count set combinational TODO: increase count when TRIG = 1;
-- Additional Comments:
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity trace_str is
    Generic(DWidth : integer :=32;
            FIFO_SIZE : integer := 1024);
    Port (
    --NATIVE PORTS
    --! CLK AND RESET SIGNALS
    CLK         : IN STD_LOGIC;
    RSTN        : IN STD_LOGIC;
    --! DATA INPUT 
    DIN         : IN STD_LOGIC_VECTOR (DWidth-1 DOWNTO 0);
    DVALID      : IN STD_LOGIC;
    READY       : IN STD_LOGIC;

    -- DATA OUTPUT SIGNALS AXIS MASTER BUS
    maxis_tdata  : out STD_LOGIC_VECTOR(DWidth -1 downto 0);
    maxis_tvalid : out STD_LOGIC;
    maxis_tkeep  : out STD_LOGIC_VECTOR(DWidth/8 - 1 downto 0);
    maxis_tready : in STD_LOGIC;
    maxis_tlast  : out STD_LOGIC;


    SBE         : IN STD_LOGIC_VECTOR  (DWidth-1 DOWNTO 0); --! Samples Before Event
    SAE         : IN STD_LOGIC_VECTOR  (DWidth-1 DOWNTO 0); --! Samples After Event

    --! Debug signals, TODO: Remove this debug signals
    debug_count : out STD_LOGIC_VECTOR(31 DOWNTO 0);
    debug_max_count : out STD_LOGIC_VECTOR(31 DOWNTO 0);

    TRIG        : IN STD_LOGIC --! Trigger Signal
   );
end trace_str;

architecture Behavioral of trace_str is

--! State machine, states declaration
--! RST_STATE: Reset condition
--! WAIT_STATE: Wait for trigger state
--! COUNT_STATE: Coung for the number of samples before and after is reached.

type state is (RST_STATE, WAIT_STATE, COUNT_STATE);
signal c_state, n_state : state; --Defining current state and next state.

signal tlast : std_logic; --! Defining AXI aux signals.
signal count, max_count : integer; --! Counter signals
signal trans : std_logic; --! Keep transfer if trigger happens before 


begin 

--!DEBUG SIGNALS TODO: REMOVE THIS SIGNALS
debug_count<=std_logic_vector(to_unsigned(count, 32));
debug_max_count<=std_logic_vector(to_unsigned(max_count, 32));

--! COUNTER process
process(CLK, RSTN, DVALID, max_count, c_state)
begin
    if RSTN ='0' then 
        count<=0;
    elsif rising_edge(CLK) then
        if c_state= COUNT_STATE then
            if count>=max_count and DVALID = '1' then
                count<=0;
            elsif  DVALID = '1' then
                count<=count+1;
            end if;
        else 
            count<=0;
        end if;
    end if;
end process;

tlast<='1' when count = (max_count -1) else '0'; --! Tlast generator

--! STATE MACHINE PROCESSES
--! Change State process;
process(CLK, RSTN)
begin
    if RSTN='0' then
     c_state<=RST_STATE;
    elsif rising_edge(CLK) then
     c_state<=n_state;
    end if;
end process;


max_count<= FIFO_SIZE when to_integer(unsigned(SBE))+to_integer(unsigned(SAE))+1 > FIFO_SIZE else
            to_integer(unsigned(SBE))+to_integer(unsigned(SAE))+1; --!max_count=SBE+SAE+1


-- Current state process
process(c_state, DVALID, maxis_tready, SBE, SAE, TRIG)
begin
    case (c_state) is
        when RST_STATE =>
            maxis_tdata   <= (others=>'0');  
            maxis_tvalid <='0';
            maxis_tkeep  <=(others=>'0');
            maxis_tlast  <='0';

            when WAIT_STATE =>
            maxis_tdata  <=DIN;  
            maxis_tvalid <='0';
            maxis_tkeep  <=(others=>'1');
            maxis_tlast  <='0';
            

        when COUNT_STATE =>
            maxis_tdata  <=DIN;  
            maxis_tvalid <=DVALID and maxis_tready;
            maxis_tkeep  <=(others=>'1');
            maxis_tlast  <=tlast;
            
    end case;
end process;


--! Next state process
process(c_state, TRIG, maxis_tready, count, max_count)
begin
    case (c_state) is
        when RST_STATE =>
            n_state<=WAIT_STATE;
        when WAIT_STATE =>
            if TRIG = '1' and maxis_tready='1' then
                n_state<=COUNT_STATE;
            else
                n_state<=WAIT_STATE;
            end if;
        when COUNT_STATE =>
            if count>=max_count then
                n_state<=WAIT_STATE;
            else
                n_state<=COUNT_STATE;
            end if;        
    end case;    
end process;
end;