----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.11.2020 18:48:19
-- Design Name: 
-- Module Name: PAS_Ctrl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--   Notes: SPI_CFG_REG[31..0]
--   SPI_CFG[31..24]  CLK_DIV
--   SPI_CFG[23..16]  ADDR
--   SPI_CFG[3]       I/O   high= input; low= output
--   SPI_CFG[2]       cont  Continous mode high= continous low= single
--   SPI_CFG[1]       cpha  phase
--   SPI_CFG[0]       pol   polarity  
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity PAS_Ctrl is
    Port (
      CLK   : IN     STD_LOGIC;                                  --system clock
      RSTN  : IN     STD_LOGIC;                                  --asynchronous reset          
      
      SPI_CFG_REG : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register
      CTRL_REG    : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register  

      
     --SPI SIGNALS

      SPI_TX  : IN     STD_LOGIC_VECTOR(16 - 1 DOWNTO 0);        --data to transmit
      SPI_RX  : OUT    STD_LOGIC_VECTOR(16 - 1 DOWNTO 0);        --data received

      spi_dio     : inout STD_LOGIC;                             --bidirectional input output 
      --    miso    : IN     STD_LOGIC;                          --master in, slave out
      spi_sclk    : out   STD_LOGIC;                             --spi clock
      spi_ss_n    : out   STD_LOGIC_VECTOR(3 -1 DOWNTO 0);       --slave select
      --    mosi    : OUT    STD_LOGIC;                          --master out, slave in
      enable_tx  : IN     STD_LOGIC;                             --initiate transaction
      busy    : OUT    STD_LOGIC;                                --busy / data ready signal

      --OTHER PINS

      A5GPIO : OUT STD_LOGIC_VECTOR(6 DOWNTO 0);
      HV_CTRL : OUT STD_LOGIC;
      LED_1   : OUT STD_LOGIC;
      RANGE_1 : OUT STD_LOGIC;
      RANGE_2 : OUT STD_LOGIC
      );                             
end PAS_Ctrl;

architecture Behavioral of PAS_Ctrl is

TYPE CFG_REG is record
    addr        : std_logic_vector(31 downto 0);
    clk_div     : std_logic_vector(31 downto 0);
    cpol        : std_logic;
    cpha        : std_logic;
    cont        : std_logic;
    io          : std_logic;
end record CFG_REG;

signal SPI : CFG_REG;

    COMPONENT spi_master IS
    GENERIC(
      slaves  : INTEGER := 3;  --number of spi slaves
      d_width : INTEGER := 16); --data bus width
    PORT(
      clock   : IN     STD_LOGIC;                             --system clock
      reset_n : IN     STD_LOGIC;                             --asynchronous reset
      enable  : IN     STD_LOGIC;                             --initiate transaction
      cpol    : IN     STD_LOGIC;                             --spi clock polarity
      cpha    : IN     STD_LOGIC;                             --spi clock phase
      cont    : IN     STD_LOGIC;                             --continuous mode command
      clk_div_i : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --system clock cycles per 1/2 period of sclk
      addr_i    : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --address of slave
      tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
      miso    : IN     STD_LOGIC;                             --master in, slave out
      sclk    : BUFFER STD_LOGIC;                             --spi clock
      ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   --slave select
      mosi    : OUT    STD_LOGIC;                             --master out, slave in
      busy    : OUT    STD_LOGIC;                             --busy / data ready signal
      rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0)); --data received
    END COMPONENT spi_master;

signal data_in, data_out : std_logic;

begin
    --OUTPUT SIGNALS 
    LED_1   <=CTRL_REG(0);
    HV_CTRL <=CTRL_REG(1);
    RANGE_1 <=CTRL_REG(2);
    RANGE_2 <=CTRL_REG(3);
    A5GPIO  <=CTRL_REG(4 DOWNTO 11);

    --register setting
    SPI.cpol    <= SPI_CFG_REG(0);
    SPI.cpha    <= SPI_CFG_REG(1);
    SPI.cont    <= SPI_CFG_REG(2);
    SPI.io      <= SPI_CFG_REG(3);
    SPI.clk_div <= x"000000" & SPI_CFG_REG(23 DOWNTO 16);
    SPI.addr    <= x"000000" & SPI_CFG_REG(31 DOWNTO 24);
    
    
    u0: spi_master
    GENERIC MAP(
      slaves  => 3, 
      d_width => 16
    )
    PORT MAP(
      clock   => CLK,
      reset_n => RSTN,
      enable  => enable_tx,
      cpol    => SPI.cpol,
      cpha    => SPI.cpha,
      cont    => SPI.cont,
      clk_div_i => SPI.clk_div, 
      addr_i    => SPI.addr,
      tx_data => SPI_TX, 
      miso    => data_in,
      sclk    =>spi_sclk,
      ss_n    =>spi_ss_n,
      mosi    =>data_out,
      busy    =>busy,
      rx_data =>SPI_RX
    );
    
    -- IOBUF: Single-ended Bi-directional Buffer
    -- 7 Series
    -- Xilinx HDL Libraries Guide, version 2012.2
    IOBUF_inst : IOBUF
    port map (
    O => data_in,   -- Buffer output
    IO => spi_dio,  -- Buffer inout port (connect directly to top-level port)
    I => data_out,  -- Buffer input
    T => SPI.io     -- 3-state enable input, high=input, low=output
    );
-- End of IOBUF_inst instantiation
    

end Behavioral;