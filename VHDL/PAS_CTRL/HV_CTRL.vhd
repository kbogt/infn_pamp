----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.11.2020 18:48:19
-- Design Name: 
-- Module Name: HV_ctrl - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--   Notes: SPI_CFG_REG[31..0]
--   SPI_CFG[31..24]  CLK_DIV
--   SPI_CFG[23..16]  ADDR
--   SPI_CFG[3]       I/O   high= input; low= output
--   SPI_CFG[2]       cont  Continous mode high= continous low= single
--   SPI_CFG[1]       cpha  phase
--   SPI_CFG[0]       pol   polarity  

--| NAME | SLAVE | Component  | Notes    |
--| ---- | ----- | ---------- | -------- |
--| VSET | SS[0] | MAX5216    | NA     |
--| VMON | SS[1] | AD4002BRMZ | MISO_0 |
--| IMON | SS[2] | AD4002BRMZ | MISO_1 |


----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity HV_ctrl is
    Port (
      CLK   : IN     STD_LOGIC;                                  --system clock
      RSTN  : IN     STD_LOGIC;                                  --asynchronous reset          
      
    -- FPGA Ports
      SPI_CFG_REG : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register
      HV_CTRL_REG    : IN     STD_LOGIC_VECTOR(31 downto 0);        --SPI Configuration register  
      
      SPI_TX  : IN     STD_LOGIC_VECTOR(31 DOWNTO 0);        --data to transmit
      SPI_RX  : OUT    STD_LOGIC_VECTOR(31 DOWNTO 0);        --data received
      
      enable_tx  : IN     STD_LOGIC;                             --initiate transaction
      busy    : OUT    STD_LOGIC;                                --busy / data ready signal

      --External ports
      HV_MISO    : IN     STD_LOGIC_VECTOR(1 downto 0);         --master in, slave out
      HV_MOSI    : OUT    STD_LOGIC;                            --master out, slave in
      HV_SCLK    : out   STD_LOGIC;                             --spi clock
      HV_CS      : out   STD_LOGIC_VECTOR(3 -1 DOWNTO 0);       --slave select      
      HV_nON      : OUT    STD_LOGIC                            -- HV ON inverse logic
      );                             
end HV_ctrl;

architecture Behavioral of HV_ctrl is

TYPE CFG_REG is record
    addr        : std_logic_vector(31 downto 0);
    clk_div     : std_logic_vector(31 downto 0);
    cpol        : std_logic;
    cpha        : std_logic;
    cont        : std_logic;
    io          : std_logic;
end record CFG_REG;

signal SPI : CFG_REG;

    COMPONENT spi_master IS
    GENERIC(
      slaves  : INTEGER := 1;  --number of spi slaves
      d_width : INTEGER := 24); --data bus width
    PORT(
      clock   : IN     STD_LOGIC;                             --system clock
      reset_n : IN     STD_LOGIC;                             --asynchronous reset
      enable  : IN     STD_LOGIC;                             --initiate transaction
      cpol    : IN     STD_LOGIC;                             --spi clock polarity
      cpha    : IN     STD_LOGIC;                             --spi clock phase
      cont    : IN     STD_LOGIC;                             --continuous mode command
      clk_div_i : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --system clock cycles per 1/2 period of sclk
      addr_i    : IN   STD_LOGIC_VECTOR(31 DOWNTO 0);         --address of slave
      tx_data : IN     STD_LOGIC_VECTOR(d_width-1 DOWNTO 0);  --data to transmit
      miso    : IN     STD_LOGIC;                             --master in, slave out
      sclk    : BUFFER STD_LOGIC;                             --spi clock
      ss_n    : BUFFER STD_LOGIC_VECTOR(slaves-1 DOWNTO 0);   --slave select
      mosi    : OUT    STD_LOGIC;                             --master out, slave in
      busy    : OUT    STD_LOGIC;                             --busy / data ready signal
      rx_data : OUT    STD_LOGIC_VECTOR(d_width-1 DOWNTO 0)); --data received
    END COMPONENT spi_master;

    COMPONENT AD4002_CNV is
      generic(tquiet1 : integer :=50);
      Port(
        rst_n     : in std_logic;
        clk       : in std_logic;
    
        addr       : in std_logic_vector(31 downto 0);
        enable_tx  : in std_logic;
        spi_cs     : in std_logic_vector(1 downto 0);
        spi_mosi   : in std_logic;
        
        cnv        : out std_logic_vector(1 downto 0);
        sdi        : out std_logic;
        enable_spi : out std_logic
      );
  end COMPONENT AD4002_CNV;

signal buff_din, data_in : std_logic;
signal data_out, busy_sig, spi_clk_sig, ss_n : std_logic_vector(1 downto 0); -- data output of two spi 
signal hv_enable_tx, mon_enable_tx, mon_mosi : std_logic;
signal mon_address : STD_LOGIC_VECTOR(31 DOWNTO 0);

begin
    --HV SIGNALS 
    HV_nON <= not HV_CTRL_REG(0); --HV on set in the lsb

    hv_enable_tx<= HV_CTRL_REG(1) and enable_tx; --Enable HV tx
    mon_address<=STD_LOGIC_VECTOR(unsigned(SPI.addr)-1); -- 
    --register setting
    SPI.cpol    <= SPI_CFG_REG(0);
    SPI.cpha    <= SPI_CFG_REG(1);
    SPI.cont    <= SPI_CFG_REG(2);
    SPI.io      <= SPI_CFG_REG(3);
    SPI.clk_div <= x"000000" & SPI_CFG_REG(23 DOWNTO 16);
    SPI.addr    <= x"000000" & SPI_CFG_REG(31 DOWNTO 24);
    
    --MUX signals
    data_in<=HV_MISO(to_integer(unsigned(mon_address))); --Chose data from MISO 0 or 1
    busy<=busy_sig(0) or busy_sig(1); --Send if any SPI is busy
    HV_MOSI<=data_out(1) when SPI.addr(1 downto 0) = "00" else
             data_out(0); --Chose spi MOSI depending of addr
    HV_SCLK<=spi_clk_sig(1) when SPI.addr(1 downto 0) = "00" else
             spi_clk_sig(0); --Chose spi MOSI--Chose SCLK
    
    

    AD4002_ctrl: AD4002_CNV
    generic map(
      tquiet1 => 50
      )
    Port map(
    rst_n      =>RSTN,
    clk        =>CLK,
    addr       =>mon_address,
    enable_tx  => enable_tx,
    spi_cs     => ss_n,
    spi_mosi   => mon_mosi,
    cnv        => HV_CS(2 downto 1),
    sdi        => data_out(0),
    enable_spi => mon_enable_tx
    );


    AD4002_spi: spi_master
    GENERIC MAP(
      slaves  => 2, 
      d_width => 18
    )
    PORT MAP(
      clock   => CLK,
      reset_n => RSTN,
      enable  => mon_enable_tx,
      cpol    => SPI.cpol,
      cpha    => SPI.cpha,
      cont    => SPI.cont,
      clk_div_i => SPI.clk_div, 
      addr_i    => mon_address, --SPI.addr -1
      tx_data => SPI_TX(17 downto 0), 
      miso    => data_in,
      sclk    =>spi_clk_sig(0),
      ss_n    =>ss_n, --slave select 1 and 2
      mosi    =>mon_mosi,
      busy    =>busy_sig(0),
      rx_data =>SPI_RX(17 downto 0)
    );

    MAX5216_SPI: spi_master
    GENERIC MAP(
      slaves  => 1, 
      d_width => 24
    )
    PORT MAP(
      clock   => CLK,
      reset_n => RSTN,
      enable  => hv_enable_tx,
      cpol    => SPI.cpol,
      cpha    => SPI.cpha,
      cont    => SPI.cont,
      clk_div_i => SPI.clk_div, 
      addr_i    => SPI.addr,
      tx_data => SPI_TX(23 downto 0), 
      miso    => data_in,
      sclk    =>spi_clk_sig(1),
      ss_n    =>HV_CS(0 downto 0), --This trick is to set std_logic_vector slave select 0
      mosi    =>data_out(1),
      busy    =>busy_sig(1),
      rx_data =>open
    );
    


end Behavioral;