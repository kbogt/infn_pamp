----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 15/05/2021
-- Design Name: TrigEdge
-- Module Name: TrigEdge - Behavioral
-- Project Name: INFN_PAMP
-- Target Devices: CIAA_ACC
-- Tool Versions: Vivado 2019.1
-- Description: Trigger signal for Edge detector
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - Unified control signals in a register
-- Additional Comments:
--
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TrigEdge is
    generic ( Trig_data_width : integer	:= 16);
    Port (  clk          : in STD_LOGIC;
            resetn       : in STD_LOGIC;
            din          : in STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
            data_valid   : in STD_LOGIC;                                        
            trig_level   : in STD_LOGIC_VECTOR (Trig_data_width - 1 downto 0);
            ext          : in STD_LOGIC;
            --! SAMPLES BEFORE AND AFTER TRIGGER
            SBT_I          : in STD_LOGIC_VECTOR(Trig_data_width -1 downto 0); 
            SAT_I          : in STD_LOGIC_VECTOR(Trig_data_width -1 downto 0);

            SBT_O          : out STD_LOGIC_VECTOR(Trig_data_width -1 downto 0); 
            SBT_SET        : out STD_LOGIC;
            SAT_O          : out STD_LOGIC_VECTOR(Trig_data_width -1 downto 0);
            --! @param CFG_REG
            --! CFG_REG[1:0] Trigger Select 
            --! CFG_REG[2] SBT_SET
            --! CFG_REG[31 DOWNTO 3] RESERVED 

            CFG_REG        : IN STD_LOGIC_VECTOR(31 DOWNTO 0);

            trig    : out STD_LOGIC);
end TrigEdge;

architecture Behavioral of TrigEdge is

    signal Threshold_new : STD_LOGIC;
    signal Threshold_old : STD_LOGIC;
    signal trig_vartual : STD_LOGIC;
    signal trig_sig : STD_LOGIC;

    signal trig_select : STD_LOGIC_VECTOR(1 downto 0);


    type state_type is (st_reset, st_waitTrig, st_DelayDelta, st_waitFifoEmpty);
    signal state   : state_type;

begin

    SBT_O<=SBT_I;
    SBT_SET<=CFG_REG(2);
    trig_select<=CFG_REG(1 DOWNTO 0);
    SAT_O<=SAT_I;
    

    process(clk, resetn, din, trig_level)
    begin
        if(rising_edge(clk)) then
            if(data_valid = '1') then           
                if(din >= trig_level) then
                    Threshold_new <= '1';
                else
                    Threshold_new <= '0';
                end if;
            end if;                             
        end if;
    end process;

    process(clk, resetn, Threshold_new)
    begin
        if(rising_edge(clk)) then
            if(data_valid = '1') then           
                Threshold_old <= Threshold_new;
            end if;
        end if;                                 
    end process;

    trig_vartual <= Threshold_new and (not Threshold_old) when ((trig_select = "01") and resetn ='1') else --00 Rising Edge
                    (not Threshold_new and Threshold_old) when ((trig_select = "00") and resetn ='1') else --01 Falling Edge
                    '1' when ((trig_select ="10") and resetn ='1') else --10 Auto
                    ext when ((trig_select ="11") and resetn='1') else --11 EXT
                    '0';
    
    trig <= trig_vartual;

end Behavioral;
