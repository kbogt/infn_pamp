----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date: 15/05/2021
-- Design Name: LED_CTRL
-- Module Name: LED_CTRL - Behavioral
-- Project Name: INFN_PAMP
-- Target Devices: CIAA_ACC
-- Tool Versions: Vivado 2019.1
-- Description: Trigger signal for Edge detector
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Revision 0.02 - Unified control signals in a register
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity LED_CTRL is
    generic ( NLEDS : integer	:= 1);
    Port (  clk          : in STD_LOGIC;
            resetn       : in STD_LOGIC;

            nc_on  : in std_logic_vector(31 downto 0); --Number of clock cycles on
            nc_off : in std_logic_vector(31 downto 0); --Number of clock cycles off
            start : in std_logic;

            
            oled : out std_logic);
end LED_CTRL;

architecture Behavioral of LED_CTRL is

    signal counter, cmax : integer;
    type state is (idle, con, coff);
    signal cstate, nstate : state;

begin
    process(clk, resetn)
    begin
        if resetn = '0' then
            cstate<=idle;
        elsif rising_edge(clk) then
            cstate<=nstate;
        end if ;
    end process;

    process(cstate)
    begin
        case cstate is
            when idle =>
                oled<='0';
            when con =>
                oled<='1';
            when coff =>
                oled<= '0';
        end case;
    end process;


    process(cstate, start, counter)
    begin
        case cstate is
            when idle =>
                if start = '1' then
                    nstate<=con;
                else
                    nstate<=idle;
                end if;
            when con => 
                if counter >=to_integer(unsigned(nc_on)) then
                    nstate<=coff;
                else 
                    nstate<=con;
                end if;
            when coff =>
                if counter >=cmax then
                    nstate<=idle;
                else
                    nstate<=coff;
                end if;
        end case;    
    end process;


    cmax<=to_integer(unsigned(nc_on)+unsigned(nc_off));
    process(clk, resetn, cstate)
    begin
        if resetn = '0' then 
            counter<=0;
        elsif rising_edge(clk) then
            if cstate= idle or counter >=cmax then
                counter<=0;
            else
                counter<=counter+1;
            end if;
        end if;
    end process;


end Behavioral;