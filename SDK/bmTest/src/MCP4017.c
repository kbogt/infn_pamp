#include "MCP4017.h"
#include "IIC_RW.h"
#include "comblock.h"
#include "xil_io.h"


int MCP4017_init(mcp4017 *ptr, u32 I2CDeviceNr, u32 MCP4017_ADDR, u32 vadj_addr){
	int status;
	ptr->I2CDeviceNr=I2CDeviceNr;
	ptr->MCP4017_ADDR=MCP4017_ADDR;
	ptr->vadj_addr=vadj_addr;
	status=xI2C_Init (ptr->I2CDeviceNr);
	return status;
}


int MCP4017_set(mcp4017 *ptr, char val){
		int status;
    	Xil_Out32(ptr->vadj_addr, 0);
    	status=xI2C_Write(ptr->I2CDeviceNr, ptr->MCP4017_ADDR, &val, 1);
    	Xil_Out32(ptr->vadj_addr, 1);
    	return status;
}

int MCP4017_off(mcp4017 *ptr){
	Xil_Out32(ptr->vadj_addr, 0);
	return 1;
}
