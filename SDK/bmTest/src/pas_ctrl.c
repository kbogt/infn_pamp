/*
 * pas_ctrl.c
 *
 *  Created on: 9/11/2020
 *      Author: mlabadm
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "xil_io.h"
#include "pas_ctrl.h"
#include "SPI_CONFIG.h"
#include "verbose.h"

int DP1, DP2;
/**pAsInit
 * Initialize picoammeter address
 *
 *
 */
void pAsInit(pAs *ptr, u32 SPI_CFG_REG_A, u32 CFG_REG_A, u32 SPI_TX_A,  u32 SPI_EN, u32 SPI_RX_A, u32 SPI_BUSY){
	ptr->ADDR.SPI_CFG_REG=SPI_CFG_REG_A;
	ptr->ADDR.CFG_REG=CFG_REG_A;
	ptr->ADDR.SPI_TX=SPI_TX_A;
	ptr->ADDR.SPI_RX=SPI_RX_A;
	ptr->ADDR.SPI_EN=SPI_EN;
	ptr->ADDR.SPI_BUSY=SPI_BUSY;
};

void strPasIn(pAs *ptr, char *cmd){
	cmd=strtok(NULL, ",");
	verbose("CTRL DP1: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP1_CTRL=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("VALUE DP1: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP1_VALUE=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("DP1 ENABLE: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP1_EN=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("CTRL DP2: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP2_CTRL=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("VALUE DP2: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP2_VALUE=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("DP2 ENABLE: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->DP2_EN=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("GPIO: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->GPIO=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("LED: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->LED=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("RANGE1: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->Range1=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("RANGE2: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->Range2=atoi(cmd);

	cmd=strtok(NULL, ",");
	verbose("HV CTRL: ");
	verbose(cmd);
	verbose("\n\r");
	ptr->HV_CTRL=atoi(cmd);
};

int pAsCtrl_Reg_wrt(pAs *ptr){
	int val=LED_1*ptr->LED+
			HVCTRL*ptr->HV_CTRL+
			RANGE_1*ptr->Range1+
			RANGE_2*ptr->Range2;
	val=val+ptr->GPIO*16;
	verbose("ADDR.CFG_REG %x, val %x\n\r",ptr->ADDR.CFG_REG, val);
	Xil_Out32(ptr->ADDR.CFG_REG, val);
	return val;
}

int pAsCtrl_pot_wrt(pAs *ptr){

}


