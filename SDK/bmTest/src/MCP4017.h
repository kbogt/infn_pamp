#ifndef __MCP4017_H_
#define __MCP4017_H_

#include "xil_io.h"

#define CIAA_MCP4017_ADDR 0x2f

typedef struct{
    u32 I2CDeviceNr;
    u32 MCP4017_ADDR;
    u32 vadj_addr;
    u32 val;
}mcp4017;

int MCP4017_init(mcp4017 *ptr, u32 I2CDeviceNr, u32 MCP4017_ADDR, u32 vadj_addr);
int MCP4017_set(mcp4017 *ptr, char val);
int MCP4017_off(mcp4017 *ptr);

#endif
