/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "xil_printf.h"
#include "comblock.h"
#include "adc500.h"
#include "xparameters.h"
#include "pas_ctrl.h"
#include "MCP4017.h"
#include "IIC_RW.h"

#include "verbose.h"
#include "sleep.h"
#define adc_cfg led0|led1

#define CIAA_MCP4017_ADDR 0x2F


int main()
{
	setVerbose(1);
	u32 val;

    //ADC INIT
    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG0, adc_cfg);

    //  Set Decimation Value
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG1, 100);


	// Set delay on shift register
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG6, 3);
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG7, 1);
	cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_OREG7, 0);


	//Init pAmp Control

//	xil_printf("I2c Init \n\r");
//	xI2C_Init (I2C_DeviceNr);
	xil_printf("Set GPIO voltage \n\r");
	mcp4017 vadj;

	MCP4017_init(&vadj,	I2C_DeviceNr, CIAA_MCP4017_ADDR, XPAR_VADJ_EN_BASEADDR);
	MCP4017_set(&vadj, 127); //Setting maximum voltage for gpio.

	pAs pAmp;

	pAsInit(&pAmp,
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_OREG2, //SPI_CFG_REG
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_OREG3, //CTRL_CFG_REG
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_OREG4, //SPI_TX_A
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_OREG5, //SPI_EN
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_IREG1, //SPI_RX_A
			XPAR_COMBLOCK_0_AXIL_BASEADDR+4*CB_IREG2//SPI_BUSY
			);

	pAmp.Range1=1;
	pAmp.Range2=0;
	pAmp.LED=1;
	pAmp.HV_CTRL=0;
	pAmp.GPIO=0;
	val=pAsCtrl_Reg_wrt(&pAmp);
//	xil_printf("Done %x\n\r", val);
//	usleep(10000000);
//
//	pAmp.Range1=0;
//	pAmp.Range2=1;
//	pAmp.LED=0;
//	val=pAsCtrl_Reg_wrt(&pAmp);
//	xil_printf("Done %x\n\r", val);

	//    Clear FIFO
		cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1);
		cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);





	    while (1){
	    for (int i=0; i<1000; i++){
	    	val=cbRead(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_VALUE);
	    	xil_printf("%d\n\r", val);
	    }

//		pAmp.Range1=!pAmp.Range1;
//		pAmp.Range2=!pAmp.Range1;
//		pAmp.LED=!pAmp.LED;
//		pAmp.HV_CTRL=0;
//		pAmp.GPIO=0;
//		val=pAsCtrl_Reg_wrt(&pAmp);
//		usleep(1000000);
	    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 1);
	    cbWrite(XPAR_COMBLOCK_0_AXIL_BASEADDR, CB_IFIFO_CONTROL, 0);
	    }



    return 0;
}
