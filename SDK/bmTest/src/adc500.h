// | Pin | Register bit| Description|
// |----| ------|-----|
// |x_to_adc_cal_fmc | Ctrl_reg_in(0)| Calibration Cycle High initiate Calibration |
// |x_to_adc_caldly_nscs_fmc | Ctrl_reg_in(1)| Calibration Delay and Serial Interface Chip Select. |
// |x_to_adc_fsr_ece_fmc | Ctrl_reg_in(2)| Full Scale Range Select and Extended Control Enable.|
// |x_to_adc_outv_slck_fmc | Ctrl_reg_in(3)| Output Voltage Amplitude and Serial Interface Clock.| 
// |x_to_adc_outedge_ddr_sdata_fmc | Ctrl_reg_in(4) |DCLK Edge Select, Double Data Rate Enable and Serial Data Input.|
// |x_to_adc_dclk_rst_fmc | Ctrl_reg_in(5) |DCLK Reset.|
// |x_to_adc_pd_fmc | Ctrl_reg_in(6)|Power Down|
// |x_to_adc_led_0 | Ctrl_reg_in(7)|LED 0|
// |x_to_adc_led_1 | Ctrl_reg_in(8)|LED 1|

#ifndef __ADC500_H_
#define __ADC500_H_

#define cal 1
#define caldly_nscs 2
#define fsr_ece 4
#define outv_slck 8
#define outedge_ddr_sdata 16
#define dclk_rst 32
#define pwr_down 64
#define led0 128
#define led1 256

// |Ctrl_reg_out(0) | x_from_adc_calrun_fmc|Calibration Running indication.|
// |Ctrl_reg_out(1) | x_from_adc_or(0)| Out Of Range output |
// |Ctrl_reg_out(31 downto 2) |(others=>'0')|

#define calrun_in 1
#define adc_or 2

#endif
