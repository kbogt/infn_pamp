/*
 * ad4002.h
 *
 *  Created on: May 30, 2021
 *      Author: kbo
 */

#ifndef AD4002_H_
#define AD4002_H_

#include "SPI_CONFIG.h"
#include "xil_io.h"

int ad4002_read(pAs_spi *spi);
int ad4002_write(pAs_spi *spi, u32 val);

#endif /* AD4002_H_ */
