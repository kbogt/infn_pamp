/**
 * @file pAmp.h
 * @addtogroup infn_pamp_v2_0
 * @author Luis Garcia(lgarcia1@ictp.it)
 * @brief pAmp functions wrapper. 
 * @version 1.00
 * @date 2021-05-13
 * 
 * 
 *
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 1.00a lg   05/18/10 First release

 ******************************************************************************/

#ifndef __pAmp_H_ /* prevent circular inclusions */
#define __pAmp_H_


/***************************** Include Files *********************************/

#include "adc500.h"
#include "xparameters.h"
#include "comblock.h"
#include "pas_ctrl.h"
#include "MCP4017.h"
#include "IIC_RW.h"
#include "verbose.h"
#include "SPI_CONFIG.h"
#include "Histogram.h"

/**************************** COMBLOCK REGISTERS DEFINITIONS *****************/
/* Comblock Base address */
#define CB_BASE XPAR_COMBLOCK_0_AXIL_BASEADDR
#define CB_RAM  XPAR_COMBLOCK_0_AXIF_BASEADDR
#define CB_MAX_MEMORY   XPAR_COMBLOCK_0_AXIF_HIGHADDR
/* Input registers *

IN_REGS
| Register | Description |
| -------  | ----------- |
| reg0 | ADC500 CTRL_REG_OUT |
| reg1 | SPI_RX |
| reg2 | SPI_BUSY |
| reg3 | pAamp_ID |
| reg4 | curr_count |
| reg5 | HISTO_DONE |
*/

/**ADC500 STATUS REGISTER **/
#define ADC500_STATUS CB_IREG0 
/** SPI status registers **/
#define SPI_RX CB_IREG1 
#define SPI_BUSY CB_IREG2
//** pAmp ID
#define pAamp_ID CB_IREG3
//** Histogram input registers
#define HISTO_CURR_COUNT CB_IREG4
#define HISTO_DONE CB_IREG5
/* Output registers 

| Register | Description |
| -------  | ----------- |
| reg0 | ADC500 Controller |
| reg1 | ADC500 Decimator N |
| reg2 | SPI_CFG_REG |
| reg3 | PAS_CTRL_REG |
| reg4 | SPI_TX |
| reg5 | Enable SPI tx |
| reg6 | SHFT_REG_N (Samples Before Event) |
| reg7 | SET N |
| reg8 | Samples After Event |
| reg9 | Synth TRIG |
| reg10 | Osciloscope samples before trigger |
| reg11 | SAT |
| reg12 | Trig_Level |
| reg13 | Oscilo_CFG |
| reg14 | HISTO_CTRL_REG |
| reg15 | HISTO_N |
*/

/*ADC500 and Decimator Registers */
#define ADC500_CTRL CB_OREG0
#define ADC500_DEC_N CB_OREG1
/*SPI CONFIG AND picoAmmeter System (PAS) control */
#define SPI_CFG_REG CB_OREG2
#define PAS_CTRL_REG CB_OREG3
#define SPI_TX CB_OREG4
#define SPI_TX_EN CB_OREG5
/*Event Tracker*/
#define SBE CB_OREG6
#define SET_SBE CB_OREG7
#define SAE CB_OREG8
#define TRIG CB_OREG9
//Osciloscope registers
#define SBT CB_OREG10
#define SAT CB_OREG11
#define Trig_Level CB_OREG12
#define Oscilo_CFG CB_OREG13
//HISTOGRAM
#define HISTO_CTRL_REG CB_OREG14
#define HISTO_N CB_OREG15

/**************************** OTHER DEFINITIONS *****************/
#define VADJ_ADDR XPAR_VADJ_EN_BASEADDR


mcp4017 VADJ;           //Vadj structure
XIicPs I2c[2];			// Structure for both I2C Devices
pAs pAmp;               //pAmp structure
pAs_spi pot[2];			//Potentiometers 0 and 1 in the pAmp board
pAs_spi temp;			//Temperature sensor
histo HGM;				//Histogram


/************************* Function Prototypes ******************************/
int ADC500_CFG(int VAL);
int SET_DEC(int N);

/* PicoAmmeter board functions */
int pAmp_init(void);
int vadj_set(int val);
int pAs_set(int pAs_reg);
int pAmp_pot(int pot, int value);
int pAmp_Gen_histo(histo *ptr, int Counts, int cont, int En);
int pAmp_Get_histo(histo *ptr, u32 *trans_buff);
#endif
