/*
 * max5216.c
 *
 *  Created on: May 27, 2021
 *      Author: mlabadm
 */

/***************************** Include Files *********************************/

#include "max5216.h"
#include "SPI_CONFIG.h"
#include "xil_types.h"
#include "xil_printf.h"


/************************** Variable Definitions *****************************/

/*****************************************************************************
* Handles power up and power down by software of the max5216
 * @param spi Picoammeter SPI pointer.
 * @param pwr power up/down configuration
 * 0 Power up and returns to its previos code setting
 * 1 DAC powers down; OUT is high impedance.
 * 2 DAC powers down; OUT connects to ground through an internal 100kI resistor.
 * 3 DAC powers down; OUT connects to ground through an internal 1kI resistor.
 *
 * @return 24 bits word to configure max5216
 * @sa https://datasheets.maximintegrated.com/en/ds/MAX5214-MAX5216.pdf
 *****************************************************************************/
u32 max5216_pwrdown(pAs_spi *spi, int pwr){
	u32 val=0;
	set_spi(spi);
	val=((pwr & 3)<<18 | 0x800000)& 0x8C0000;
	spi_tx(spi, val);
	send_spi(spi,0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	return val;
}


/*****************************************************************************
* Sets value of max5216
 * @param spi Picoammeter SPI pointer.
 * @param val, 16 bits value to set  the DAC
 * @return 24 bits word to configure max5216
 * @sa https://datasheets.maximintegrated.com/en/ds/MAX5214-MAX5216.pdf
 *****************************************************************************/
u32 max5216_write(pAs_spi *spi, u32 val){
	u32 res=(((val&0xFFFF)<<6) | 0x400000)& 0x7FFFC0;
	set_spi(spi);
	spi_tx(spi, res);
	send_spi(spi,0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	return res ;
}
