/**
 * @file pAmp.c
 * @addtogroup infn_pamp_v2_0
 * @author Luis Garcia(lgarcia1@ictp.it)
 * @brief pAmp functions wrapper. 
 * @version 1.00
 * @date 2021-05-13
 * 
 * 
 *
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 1.00a lg   05/18/10 First release

 ******************************************************************************/

/***************************** Include Files *********************************/

#include "pAmp.h"
#include "MCP4017.h"
#include "SPI_CONFIG.h"

/************************** Variable Definitions *****************************/

/*****************************************************************************/
/**
 * This function sets the ADC500_CTRL register in the DRIVER. 
 *
 * @param	VAL ADC500 config value
 * @return
 * 		- ADC500 status register
 *
 *****************************************************************************/

int ADC500_CFG(int VAL){
    int status;
    cbWrite(CB_BASE, ADC500_CTRL, VAL);
    status=cbRead(CB_BASE, ADC500_STATUS);
    return status;
}

/*****************************************************************************/
/**
 * This function sets a Decimation value in the Decimation block.
 *
 * @param	N  Decimation value set
 * @return
 * 		N returns decimation value if success,
 *      0 if Fails
 *
 *****************************************************************************/
int SET_DEC(int N){
    if (N>0){
        cbWrite(CB_BASE, ADC500_DEC_N, N);
        return N;
    }
    else
        return 0;
}

/*****************************************************************************/
/**
 * This function Initializes the pAmp Board and I2C drivers.
 * Initialize the MCP4017 and the I2C driver
 * Sets pAmp Control registers
 * @return
 * 		2 if Success
 *      0 if Fails
 *
 *****************************************************************************/
int pAmp_init(void){
    int status=0;
    status=MCP4017_init(&VADJ, I2C_DeviceNr, CIAA_MCP4017_ADDR, VADJ_ADDR);//Initialize MCP4017
    status+=MCP4017_set(&VADJ, 100); //Setting maximum voltage for gpio.
    pAsInit(&pAmp,
		CB_BASE+4*SPI_CFG_REG, //SPI_CFG_REG
		CB_BASE+4*PAS_CTRL_REG, //CTRL_CFG_REG
		CB_BASE+4*SPI_TX, //SPI_TX_A
		CB_BASE+4*SPI_TX_EN, //SPI_EN
		CB_BASE+4*SPI_RX, //SPI_RX_A
		CB_BASE+4*SPI_BUSY//SPI_BUSY
        );

    //Setup SPI components address
    spi_device(&pot[0], spi_clkdiv,  POT1_ADDR, AD5292_spicfg);
    spi_device(&pot[1], spi_clkdiv,  POT2_ADDR, AD5292_spicfg);
    spi_device(&temp, spi_clkdiv,  TEMP_ADDR, LM95071_spicfg);

    pAsInit_spi(&pot[0], &pAmp);
    pAsInit_spi(&pot[1], &pAmp);
    pAsInit_spi(&temp, &pAmp);

    //Power on both AD5292
    AD5292_SHTDWN(&pot[0], 0);
    AD5292_SHTDWN(&pot[1], 0);

    //Initializing Histogram
    Histo_init(&HGM,
        CB_BASE+4*HISTO_CTRL_REG,
        CB_BASE+4*HISTO_N,
        CB_BASE+4*HISTO_CURR_COUNT,
        CB_BASE+4*HISTO_DONE,
        CB_RAM,
        (CB_MAX_MEMORY-CB_RAM)/4);

    status+=1;
    return status;
}

/*****************************************************************************/
/**
 * This function sets the VADJ output voltage
 * @param val 0-127 to set the MCP4017 more info in 
 * @sa https://gitlab.com/kbogt/infn_pamp/-/wikis/VADJ-Configuration
 * @return
 * 		1 if Success
 *      0 if Fails
 *
 *****************************************************************************/
int vadj_set(int val){
	MCP4017_init(&VADJ, I2C_DeviceNr, CIAA_MCP4017_ADDR, VADJ_ADDR);//Initialize MCP4017
    int status;
    status=MCP4017_set(&VADJ, val&0x7f);
    return status;
}

/*****************************************************************************/
/**
 * This function sets the VADJ output voltage
 * @param val 0-127 to set the MCP4017 more info in 
 * @sa https://gitlab.com/kbogt/infn_pamp/-/wikis/VADJ-Configuration
 * @return
 * 		1 if Success
 *      0 if Fails
 *
 *****************************************************************************/
int pAs_set(int pAs_reg){
    int status;
    pAmp.LED    = (pAs_reg&LED_1);
    pAmp.HV_CTRL= (pAs_reg&HVCTRL)>>1;
    pAmp.Range1 = (pAs_reg&RANGE_1)>>2;
	pAmp.Range2 = (pAs_reg&RANGE_2)>>3;
	pAmp.GPIO   = (pAs_reg&A5GPIO)>>4;
    status=pAsCtrl_Reg_wrt(&pAmp);
    return status;
}

/*****************************************************************************/
/**
 * This function sets the AD5292 potentiometers of the pAmp board
 * @param pot 0-2 Selects pot number, 0 is power on/off mode,
 * @param val 0-1024 to set the MCP4017 more info in.
 * If power on/off mode value =0 is normal mode, value =1 is power off
 * @sa https://gitlab.com/kbogt/infn_pamp/-/wikis/VADJ-Configuration
 * @return
 * 		1 if Success
 *      0 if Fails
 *
 *****************************************************************************/
int pAmp_pot(int Npot, int value){
	int status;
	verbose("Npot %d, Value %d \n\r", Npot, value);
	if ((Npot>0) && Npot<3){
		status=AD5292_wRDAC(&pot[Npot-1], value);
	}
	else{
		verbose("Wrong parameters for pAmp_pot Npot %u and value %u", Npot, value);
		status=0;
	}
	return status;
}

/*****************************************************************************/
/**
 * This function Generates a histogram using HISTOGRAM functions
 * @param ptr Histogram pointer
 * @param Counts Number of counts to generate histogram
 * @param Cont Continous acquisition or single acquisition.
 * @param En Enable histogram count
 *
 *****************************************************************************/

int pAmp_Gen_histo(histo *ptr, int Counts, int cont, int En){
	int status=0;
	//Clean memory unless is set in continuous acquisition mode.
	if (cont!=1){
		verbose("Cleaning memory... ");
		status=clean_TDPR(ptr);
		verbose("Done, status %x \r\n",status);
	}
	verbose("Setting histogram for %d counts:\t",Counts);
	status= Histo_SetCounts(ptr, Counts);
	verbose("Done, status %x \r\n",status);
	verbose("Initialize count \t");
	status=Histo_count_enable(ptr, En);
	verbose("Done, status %x \r\n",status);
	return status;
}
/*****************************************************************************/
/**
 * This function Gets histogram data from memory using HISTOGRAM functions
 * @param ptr Histogram pointer
 * @param trans_buff Histogram transmission buffer
 *
 *****************************************************************************/

int pAmp_Get_histo(histo *ptr, u32 *trans_buff){
	trans_buff[0]=Histo_rfm(ptr, &trans_buff[3]); //Status of histogram is done
	verbose("Histo is done %d \n\r",trans_buff[0]);
	trans_buff[2]=ptr->Memory_bins; //Send number of bins
	verbose("Histogram Read From Memmory \n\r");
	trans_buff[1]=Histo_Get_Curr_Counts(ptr);
	verbose("Current counts %d \n\r",trans_buff[1]);
	return 1;
}
