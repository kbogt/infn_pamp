/*
 * ECAL.h
 *
 *  Created on: May 27, 2021
 *      Author: L.Garcia 
 */

#ifndef SRC_ECAL_HV_H_
#define SRC_ECAL_HV_H_

//Include dependences libraries
#include "SPI_CONFIG.h"
#include "xparameters.h"
#include "comblock.h"
#include "xil_types.h"
#include "xil_printf.h"

#define ECAL_SPI_CFG_REG XPAR_COMBLOCK_1_AXIL_BASEADDR+4*CB_OREG4
#define ECAL_PMT_ADDR XPAR_COMBLOCK_1_AXIL_BASEADDR+4*CB_OREG5
#define ECAL_SPI_TX XPAR_COMBLOCK_1_AXIL_BASEADDR+4*CB_OREG6
#define ECAL_enable_tx XPAR_COMBLOCK_1_AXIL_BASEADDR+4*CB_OREG7
#define ECAL_SPI_BUSY XPAR_COMBLOCK_1_AXIL_BASEADDR+4*CB_IREG2


//SPI pointer
pAs_spi ecal_dac={
//     //Base address of setup registers
ECAL_SPI_CFG_REG,// 	u32 SPI_CFG_REG;
ECAL_SPI_TX,// 	u32 SPI_TX;
0,// 	u32 SPI_RX;
ECAL_enable_tx,// 	u32 SPI_EN;
ECAL_SPI_BUSY,// 	u32 SPI_BUSY;
//    //spi configuration parameters
200,//    u8 clk_div;
0,//    u8 addr;
0x01//    u8 config;
};

pAs_spi ecal_pmt={
//     //Base address of setup registers
ECAL_SPI_CFG_REG,// 	u32 SPI_CFG_REG;
ECAL_SPI_TX,// 	u32 SPI_TX;
0,// 	u32 SPI_RX;
ECAL_enable_tx,// 	u32 SPI_EN;
ECAL_SPI_BUSY,// 	u32 SPI_BUSY;
//    //spi configuration parameters
200,//    u8 clk_div;
0,//    u8 addr;
0x11//    u8 config;
};



int ECAL_set_ch(u32 ch){
	u32 res=(ch & 0xf); //PMT address
	u32 addr=(ch>>4)&0xf; //SPI slave select for SPI.

    set_spi(&ecal_pmt);
	regWrite(ECAL_PMT_ADDR, res); //COMBLOCK PMT address
    ecal_pmt.addr=addr;
    ecal_dac.addr=addr;
	send_spi(&ecal_pmt,0);
    	int i=0;
    	while(spi_busy(&ecal_pmt)){
    		if (i>300000000){ //Timeout on spi busy
    			xil_printf("SPI Busy timeout \n\r");
    			return 0;
    		}
    		i++;
    	}
    	return res ;
	return ch;
}

/*****************************************************************************
* Handles power up and power down by software of the ECAL
 * @param ch Picoammeter SPI pointer.
 * @param pwr power up/down configuration
 * 0 Power up and returns to its previos code setting
 * 1 DAC powers down; OUT connects to ground through an internal 1kI resistor.
 * 2 DAC powers down; OUT connects to ground through an internal 100kI resistor.
 * 3 DAC powers down; OUT is high impedance.
 *
 * @return 16 bits word to configure ECAL
 * @sa https://datasheets.maximintegrated.com/en/ds/MAX5214-ECAL.pdf
 *****************************************************************************/
u32 ECAL_pwrdown(u32 ch, int pwr){
	u32 val=0;
    ECAL_set_ch(ch);
	set_spi(&ecal_dac);
	val=((pwr & 3)<<12 | 0x8000) & 0x8C00;
	spi_tx(&ecal_dac, val);
	send_spi(&ecal_dac,0);
	int i=0;
	while(spi_busy(&ecal_dac)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	return val;
}


/*****************************************************************************
* Sets value of ECAL
 * @param ch Picoammeter SPI pointer.
 * @param spi Picoammeter SPI pointer.
 * @param val, 16 bits value to set  the DAC
 * @return 24 bits word to configure ECAL
 * @sa https://datasheets.maximintegrated.com/en/ds/MAX5214-ECAL.pdf
 *****************************************************************************/
u32 ECAL_write(u32 ch, u32 val){
	ECAL_set_ch(ch);
	u32 res=val&0xfff;
	set_spi(&ecal_dac);
	spi_tx(&ecal_dac, res);
	send_spi(&ecal_dac,0);
	int i=0;
	while(spi_busy(&ecal_dac)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return -1;
		}
		i++;
	}
	return res ;
}

u32 ECAL_write_all(u32 N, u32 val){
	for (int i =0; i<N; i++){
		if(ECAL_write(i, val) < 0)
			return -1;
	}
}



//Function Definitions
int ECAL_set_ch(u32 ch);
u32 ECAL_pwrdown(u32 ch, int pwr);
u32 ECAL_write(u32 ch, u32 val);
u32 ECAL_write_all(u32 N, u32 val);

#endif /* SRC_ECAL_H_ */
