/*
 * hvpwrsply.h
 *
 *  Created on: May 27, 2021
 *      Author: mlabadm
 */

#ifndef SRC_HVPWRSPLY_H_
#define SRC_HVPWRSPLY_H_

/***************************** Include Files *********************************/

#include "xparameters.h"
#include "comblock.h"
#include "verbose.h"
#include "SPI_CONFIG.h"
#include "max5216.h"

#define HV_CB_BASE XPAR_COMBLOCK_1_AXIL_BASEADDR

/** SPI status registers **/
#define HV_SPI_RX CB_IREG0
#define HV_SPI_BUSY CB_IREG1
#define HV_SPI_CFG_REG CB_OREG0
#define HV_CTRL_REG CB_OREG1
#define HV_SPI_TX CB_OREG2
#define HV_SPI_TX_EN CB_OREG3




int hv_set(u32 val);
int hv_pwrDown(u32 val);

int vmon_read(void);
int imon_read(void);



#endif /* SRC_HVPWRSPLY_H_ */
