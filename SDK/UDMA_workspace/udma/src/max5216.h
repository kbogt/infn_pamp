/*
 * max5216.h
 *
 *  Created on: May 27, 2021
 *      Author: mlabadm
 */

#ifndef SRC_MAX5216_H_
#define SRC_MAX5216_H_

//Include dependences libraries
#include "SPI_CONFIG.h"

//SPI pointer
pAs_spi max5216;

//Function Definitions
u32 max5216_pwrdown(pAs_spi *spi, int pwr);
u32 max5216_write(pAs_spi *spi, u32 val);

#endif /* SRC_MAX5216_H_ */
