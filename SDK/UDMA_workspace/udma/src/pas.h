/*
 * PAS.h
 *
 *      Author: LGARCIA
 */

#ifndef PAS_H_
#define PAS_H_

#include "comblock.h"

#define SPI_CFG_REG CB_OREG1

/*Masks definition*/
#define LED_1 0x001;
#define HC_CTRL 0x002;
#define pa_Range_Q2 0x004;
#define pa_Range_Q3 0x008;
#define GPIO 0xFF0;

#endif