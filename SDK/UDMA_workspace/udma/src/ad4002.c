/*
 * ad4002.c
 *
 *  Created on: May 30, 2021
 *      Author: kbo
 */

/***************************** Include Files *********************************/

#include "ad4002.h"

/*Read values from the AD4002 adc
 * @param spi pAs_spi Spi pointer
 * @return spi TX value
 */

int ad4002_read(pAs_spi *spi){
	set_spi(spi);
	spi_tx(spi, 0xffffff); //Set TX to 1
	send_spi(spi,0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	send_spi(spi,0);
	i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	u32 val=spi_rx(spi);
	return val;
}

int ad4002_write(pAs_spi *spi, u32 val){
	u32 res=0;
	set_spi(spi);
	spi_tx(spi, res);
	send_spi(spi,0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}
	return res ;
}
