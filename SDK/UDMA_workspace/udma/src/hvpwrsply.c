/*
 * hvpwrsply.c
 *
 *  Created on: May 27, 2021
 *      Author: mlabadm
 */
#include "hvpwrsply.h"
#include "max5216.h"
#include "ad4002.h"
#include "xil_io.h"

pAs_spi HVadj_spi={
		//Base address of setup registers
		HV_CB_BASE+4*HV_SPI_CFG_REG,
		HV_CB_BASE+4*HV_SPI_TX,
		HV_CB_BASE+4*HV_SPI_RX,
		HV_CB_BASE+4*HV_SPI_TX_EN,
		HV_CB_BASE+4*HV_SPI_BUSY,
	   //spi configuration parameters
		spi_clkdiv,
		HV_ADJ,
		MAX5216_spicfg
};

pAs_spi vmon_spi={
		//Base address of setup registers
		HV_CB_BASE+4*HV_SPI_CFG_REG,
		HV_CB_BASE+4*HV_SPI_TX,
		HV_CB_BASE+4*HV_SPI_RX,
		HV_CB_BASE+4*HV_SPI_TX_EN,
		HV_CB_BASE+4*HV_SPI_BUSY,
	   //spi configuration parameters
		spi_clkdiv,
		VMON,
		AD4002_RD_spicfg
};

pAs_spi imon_spi={
		//Base address of setup registers
		HV_CB_BASE+4*HV_SPI_CFG_REG,
		HV_CB_BASE+4*HV_SPI_TX,
		HV_CB_BASE+4*HV_SPI_RX,
		HV_CB_BASE+4*HV_SPI_TX_EN,
		HV_CB_BASE+4*HV_SPI_BUSY,
	   //spi configuration parameters
		spi_clkdiv,
		IMON,
		AD4002_RD_spicfg
};

int hv_set(u32 val){
	int rd=Xil_In32(HV_CB_BASE+4*HV_CTRL_REG);
	Xil_Out32(HV_CB_BASE+4*HV_CTRL_REG, rd|0x2);
	int status=max5216_write(&HVadj_spi, val);
	Xil_Out32(HV_CB_BASE+4*HV_CTRL_REG, rd&0x1);
	return status;
}

int hv_pwrDown(u32 val){
	int rd=Xil_In32(HV_CB_BASE+4*HV_CTRL_REG);
	Xil_Out32(HV_CB_BASE+4*HV_CTRL_REG, rd|0x2);
	int status=max5216_pwrdown(&HVadj_spi, val);
	Xil_Out32(HV_CB_BASE+4*HV_CTRL_REG, rd&0x1);
	return status;
}

int vmon_read(void){
	int rd=ad4002_read(&vmon_spi);
	return rd;
}

int imon_read(void){
	int rd=ad4002_read(&imon_spi);
	return rd;
}
