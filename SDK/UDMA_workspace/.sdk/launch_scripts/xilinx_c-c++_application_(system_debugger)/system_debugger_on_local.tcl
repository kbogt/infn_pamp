connect -url tcp:194.12.182.175:3121
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-SMT2 210251ADE546"} -index 1
rst -system
after 3000
targets -set -nocase -filter {name =~"APU*" && jtag_cable_name =~ "Digilent JTAG-SMT2 210251ADE546"} -index 1
loadhw -hw /media/kbo/DATA/my_gits/infn_pamp/SDK/UDMA_workspace/Top_wrapper_hw_platform_0/system.hdf -mem-ranges [list {0x40000000 0xbfffffff}]
configparams force-mem-access 1
configparams force-mem-access 0
