/**
 * @file pAmp.h
 * @addtogroup infn_pamp_v2_0
 * @author Luis Garcia(lgarcia1@ictp.it)
 * @brief pAmp functions wrapper. 
 * @version 1.00
 * @date 2021-05-13
 * 
 * 
 *
 * MODIFICATION HISTORY:
 *
 * Ver   Who  Date     Changes
 * ----- ---- -------- -------------------------------------------------------
 * 1.00a lg   05/18/10 First release

 ******************************************************************************/

#ifndef __pAmp_H_ /* prevent circular inclusions */
#define __pAmp_H_


/***************************** Include Files *********************************/

#include "adc500.h"
#include "xparameters.h"
#include "comblock.h"
#include "pas_ctrl.h"
#include "MCP4017.h"
#include "IIC_RW.h"
#include "verbose.h"
#include "SPI_CONFIG.h"

/**************************** COMBLOCK REGISTERS DEFINITIONS *****************/
/* Comblock Base address */
#define CB_BASE XPAR_COMBLOCK_0_AXIL_BASEADDR
/* Input registers *

IN_REGS
| Register | Description |
| -------  | ----------- |
| reg0 | ADC500 CTRL_REG_OUT |
| reg1 | SPI_RX |
| reg2 | SPI_BUSY |
*/

/**ADC500 STATUS REGISTER **/
#define ADC500_STATUS CB_IREG0 
/** SPI status registers **/
#define SPI_RX CB_IREG1 
#define SPI_BUSY CB_IREG2

/* Output registers 

| Register | Description |
| -------  | ----------- |
| reg0 | ADC500 Controller |
| reg1 | ADC500 Decimator N |
| reg2 | SPI_CFG_REG |
| reg3 | PAS_CTRL_REG |
| reg4 | SPI_TX |
| reg5 | Enable SPI tx |
| reg6 | SHFT_REG_N (Samples Before Event) |
| reg7 | SET N |
| reg8 | Samples After Event |
| reg9 | Synth TRIG |
*/

/*ADC500 and Decimator Registers */
#define ADC500_CTRL CB_OREG0
#define ADC500_DEC_N CB_OREG1
/*SPI CONFIG AND picoAmmeter System (PAS) control */
#define SPI_CFG_REG CB_OREG2
#define PAS_CTRL_REG CB_OREG3
#define SPI_TX CB_OREG4
#define SPI_TX_EN CB_OREG5
/*Event Tracker*/
#define SBE CB_OREG6
#define SET_SBE CB_OREG7
#define SAE CB_OREG8
#define TRIG CB_OREG9

/**************************** OTHER DEFINITIONS *****************/
#define VADJ_ADDR XPAR_VADJ_EN_BASEADDR


mcp4017 VADJ;           //Vadj structure
XIicPs I2c[2];			// Structure for both I2C Devices
pAs pAmp;               //pAmp structure
pAs_spi pot[2];			//Potentiometers 0 and 1 in the pAmp board
pAs_spi temp;			//Temperature sensor


/************************* Function Prototypes ******************************/
int ADC500_CFG(int VAL);
int SET_DEC(int N);

/* PicoAmmeter board functions */
int pAmp_init(void);
int vadj_set(int val);
int pAs_set(int pAs_reg);
int pAmp_pot(int pot, int value);
#endif
