pAmp initialization:

Prepare pAmp Address for the SPI and the generic GPIO signals of the pAmp

```
#include pas_ctrl.h

pAs pAmp; //Control pointer

void pAsInit(pAs *ptr, u32 SPI_CFG_REG_A, u32 CFG_REG_A, u32 SPI_TX_A,  u32 SPI_EN, u32 SPI_RX_A, u32 SPI_BUSY)
```

1. **ptr** Control pointer.
2. **SPI_CFG_REG_A** SPI config register address *e.g. XPAR_COMBLOCK_0_AXIL_BASEADDR+CB_OREG2.*
3. **CFG_REG_A** General config register address *e.g. XPAR_COMBLOCK_0_AXIL_BASEADDR+CB_OREG3.*
4. **SPI_TX_A** SPI TX address register.
5. **SPI_EN** SPI enable address
6. **SPI_RX_A** SPI RX address.
7. **SPI_BUSY** SPI busy signal. 
