/*
 * SPI_CONFIG.h
 *
 *  Created on: 17/02/2020
 *      Author: mlabadm
 */

#ifndef SRC_SPI_CONFIG_H_
#define SRC_SPI_CONFIG_H_

#include "xil_types.h"

#define spi_enable  0x1
#define spi_disable 0x0

//Configuration parameters for set_spi function
#define spi_cpol	0x1
#define spi_cpha	0x2
#define spi_cont	0x4
#define spi_read  0x8

#define SPI_SUCCESS 0
#define SPI_ERROR 1

//CTRL register pasive ios
#define LED_1 	0x00000001
#define HVCTRL  0x00000002
#define RANGE_1	0x00000004
#define RANGE_2 0x00000008
#define A5GPIO  0x00000ff0

#define TEMP_ADDR 0
#define POT1_ADDR 1
#define POT2_ADDR 2

#define spi_clkdiv 200

#define AD5292_spicfg 2 //cpol 0 cpha 1

#define LM95071_spicfg spi_read //cpol 0, cpha 0

#define POT1_CFG ((spi_clkdiv & 0xff)<<24) | ((POT1_ADDR & 0xff)<<16) | AD5292_spicfg
#define POT2_CFG ((spi_clkdiv & 0xff)<<24) | ((POT2_ADDR & 0xff)<<16) | AD5292_spicfg
#define TEMP_CFG ((spi_clkdiv & 0xff)<<24) | ((TEMP_ADDR & 0xff)<<16) | LM95071_spicfg


typedef struct {
	//Base address of setup registers
	u32 SPI_CFG_REG;
	u32 SPI_TX;
	u32 SPI_RX;
	u32 SPI_EN;
	u32 SPI_BUSY;
   //spi configuration parameters
   u8 clk_div;
   u8 addr;
   u8 config;
} pAs_spi;

void spi_device(pAs_spi *spi, u8 clk_div, u8 addr, u8 config);
void init_spi(pAs_spi *spi, u32 spi_cfg_addr, u32 spi_tx_addr, u32 spi_rx_addr, u32 spi_en_addr, u32 spi_busy_addr);
int set_spi(pAs_spi *spi);
int send_spi(pAs_spi *spi,int hold);
int spi_busy(pAs_spi *spi);
int spi_tx(pAs_spi *spi,u32 data);
int spi_rx(pAs_spi *spi);


/*Return the configuration and data value to send via SPI for a AD5292
 * Check datasheet for command and data values
 * returns val: concatenated command and data packaged and ready for transmission.
 */
u16 AD5292_data(u8 command, u16 data);
int AD5292_RDACEn(pAs_spi *spi, u32 enable);
int AD5292_wRDAC(pAs_spi *spi, u16 data);
int AD5292_SHTDWN(pAs_spi *spi, int offOn);

static inline void regWrite(UINTPTR baseaddr, u32 value) {
   volatile u32 *addr = (volatile u32 *)(baseaddr);
   *addr = value;
}

static inline u32 regRead(UINTPTR baseaddr) {
   return *(volatile u32 *)(baseaddr);
}



#endif /* SRC_SPI_CONFIG_H_ */

