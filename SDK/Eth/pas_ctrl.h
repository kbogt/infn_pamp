/*
 * pas_ctrl.h
 *
 *  Created on: 9/11/2020
 *      Author: mlabadm
 */

#ifndef SRC_PAS_CTRL_H_
#define SRC_PAS_CTRL_H_

#include <stdio.h>
#include <string.h>
#include "xil_io.h"
#include "spi.h"


typedef struct {
	//Base address of setup registers
	u32 SPI_CFG_REG;
	u32 CFG_REG;
	u32 SPI_TX;
	u32 SPI_RX;
	u32 SPI_EN;
	u32 SPI_BUSY;
} pAs_addr;

typedef struct{
	pAs_addr ADDR;
	u16 DP1_VALUE;
	u16 DP1_CTRL;
	u16 DP1_EN;
	u16 DP2_VALUE;
	u16 DP2_CTRL;
	u16 DP2_EN;
	u8  LED;
	u8	Range1;
	u8	Range2;
	u8  HV_CTRL;
	u8  GPIO;
} pAs;

pAs pamp;


void pAsInit(pAs *ptr, u32 SPI_CFG_REG_A, u32 CFG_REG_A, u32 SPI_TX_A,  u32 SPI_EN, u32 SPI_RX_A, u32 SPI_BUSY);
void strPasIn(pAs *ptr, char *cmd);
int pAsCtrl_Reg_wrt(pAs *ptr);

#endif /* SRC_PAS_CTRL_H_ */
