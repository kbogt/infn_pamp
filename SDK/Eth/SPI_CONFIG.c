/*
 * SPI_CONFIG.c
 *
 *  Created on: 17/02/2020
 *      Author: mlabadm
 */

#include "SPI_CONFIG.h"
#include "xil_types.h"
#include "xparameters.h"

/*Creates spi device, to be used with the SPI master block
*Returns a spi component;
*/
pAs_spi spi_device(u8 clk_div, u8 addr, u8 config){
	pAs_spi spi;
	spi->clk_div=clk_div;
	spi->addr;
	spi->config=config;
	return spi;
}


//Initialize spi device and load SPI block registers
void init_spi(pAs_spi *spi, u32 spi_cfg_addr, u32 spi_tx_addr, u32 spi_rx_addr, u32 spi_en_addr, u32 spi_busy_addr){
	spi->SPI_CFG_REG=spi_cfg_addr;
	spi->SPI_TX=spi_tx_addr;
	spi->SPI_RX=spi_rx_addr;
	spi->SPI_EN=spi_en_addr;
	spi->SPI_BUSY=spi_busy_addr;
}

/*Setup SPI configuration parameters into a register
 *
 */
int set_spi(pAs_spi *spi){
	u32 cfg_reg=(((spi.addr & 0xff)<<24)| (spi.clk_div & 0xff) <<16) | (spi.config & 0xf);
	regWrite(spi->SPI_CFG_REG, cfg_reg); //SPI_TX
	return cfg_reg;
}


/* Send enable signal to the block to start SPI transaction using the configuration
 * set with "set_spi" function.
 * if hold = 1 will hold the enable flag.
 */
int send_spi(pAs_spi *spi,int hold){
	regWrite(spi->SPI_EN, spi_enable); //SPI_CFG_REG
	if (hold == 1)
		return SPI_SUCCESS;
	else
		regWrite(spi->SPI_EN, spi_disable); //SPI_CFG_REG
	return SPI_SUCCESS;
}

/* Read busy register on spi wrapper
 *
 */
int spi_busy(pAs_spi *spi){
	int busy=regRead(spi->SPI_BUSY); //SPI BUSY
	return busy;
}

/* Set transfer data register in spi_wrapper
 * returns SPI_ERROR if transfer is busy, success if is not busy.
 */
int spi_tx(pAs_spi *spi, u32 data){
	int busy=spi_busy(spi);
	if (busy==1){
		xil_printf("The device is busy");
		return SPI_ERROR;
	}
	else{
		regWrite(spi->SPI_TX, data);
		return SPI_SUCCESS;
}

/*Read receive data register form spi_wrapper
 *
 */
int spi_rx(pAs_spi *spi){
	int data=regRead(spi->SPI_RX);
	return data;
}

/*Return the configuration and data value to send via SPI for a AD5292
 * Check datasheet for command and data values
 * returns val: concatenated command and data packaged and ready for transmission.
 */
u16 AD5292_data(u8 command, u16 data){
		u16 val=0;
		val=(command & 0xf)<<10 | (data & 0x3ff);
		return val;
}


int AD5292_RDACEn(pAs_spi *spi, u32 enable){
	int setup=set_spi(spi);
	int status=0;
	u32 plc;
	if (enable=1){
		plc=AD5292_data(6, 2); //Enable RDAC write
		status=1;
	}
	else{
		p1c=AD5292_data(6, 0); //Enable RDAC write
		status=0;
	};
	spi_tx(spi,p1c);
	send_spi(spi, 0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return -1;
		}
		i++;
	}
	return status;
}


/*Write contents of serial data to RDAC
*
*
*/
int AD5292_wRDAC(pAs_spi *spi, u8 clkdiv, u8 slv_addr, u16 data){
	int setup=set_spi(spi);
	u32 p1c=AD5292_data(2, data); //Enable RDAC load
	spi_tx(spi, p1c);
	send_spi(spi, 0);
	int i=0;
	while(spi_busy(spi)){
		if (i>300000000){ //Timeout on spi busy
			xil_printf("SPI Busy timeout \n\r");
			return 0;
		}
		i++;
	}


}


