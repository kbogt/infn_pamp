#ifndef __MCP4017_H_
#define __MCP4017_H_

#include "xil_io.h"

#define MCP4017_ADDR 0x2f

typedef struct{
    u32 I2C_DeviceNr;
    u32 MCP4017_ADDR;
    u32 vadj_addr;    
}MCP4017;

int MCP4017_init(MCP4017 *ptr, u32 I2C_DeviceNr, u32 MCP4017_ADDR, u32 vadj_addr);
void MCP4017_set(char val);
void MCP4017_off(void);

#endif
