#include "MCP4017.h"
#include "IIC_RW.h"
#include "comblock.h"
#include "xil_io.h"


int MCP4017_init(u32 cb_addr, u32 vadj_cb_reg){
	cb_baddr=cb_addr;
	vadj_reg=vadj_cb_reg;
	return cb_baddr+vadj_cb_reg;
}


void MCP4017_set(char val){
    	cbWrite(cb_baddr, vadj_reg, 0);
    	xI2C_Write(I2C_DeviceNr, MCP4017_ADDR, &val, 1);
    	cbWrite(cb_baddr, vadj_reg, 1);
}

void MCP4017_off(void){
	cbWrite(cb_baddr, vadj_reg, 0);
}
